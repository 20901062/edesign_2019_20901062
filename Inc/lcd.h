/*
 * lcd.h
 *
 *  Created on: 24 Apr 2019
 *      Author: 20901062
 */

#ifndef LCD_H_
#define LCD_H_

void lcd_init();
void lcd_write_to_pins(int RS, int RNW, int DB4, int DB5, int DB6, int DB7);
void lcd_enable_disable();
void lcd_charHandler(uint8_t c);
void lcd_stringHandler(char* s);
void lcd_displayHandler();


#endif /* LCD_H_ */
