/*
 * Parse.h
 *
 *  Created on: 07 Mar 2019
 *      Author: 20901062
 */

#ifndef PARSE_H_
#define PARSE_H_

void parse_init();
void parse_main();
void parse_output(int runtime, int cur, float vol);


#endif /* PARSE_H_ */
