/*
 * UART.h
 *
 *  Created on: 05 Mar 2019
 *      Author: 20901062
 */

#ifndef UART_H_
#define UART_H_

 void user_uart(UART_HandleTypeDef * huart1);
 void user_uart_init(UART_HandleTypeDef * huart1);
 void deactivate_burn_signal();


#endif /* UART_H_ */
