/*
 * Globals.h
 *
 *  Created on: 29 Mar 2019
 *      Author: 20901062
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

//Basic functions
int cmp_arr(uint8_t * valid, uint8_t * input, int start, int end);
void clear_arr(uint8_t * arr);
int arr_length(uint8_t * arr);
int calc_checksum(uint8_t * arr);
int hex2chksum (uint8_t * arr);
int ascii_to_int(uint8_t n);
float str_to_float(uint8_t * arr);

//Buffer variables

uint8_t save[100];
uint8_t bckup[100];
char transmit[100];
uint8_t lon[12];
uint8_t lat[12];
uint8_t t[6];
uint8_t alt[8];
uint8_t chksum[2];

//North/East
int N;
int E;

char burn_char;
//uint8_t time

//Position Variables

float latf;
float lonf;
float altf;

//Current & voltage sensing

float current;
float voltage;

int current_2;

float step_voltage[50];
float step_current[50];

//Variables

 int flag_fullmsg;
 int flag_GPGGA;
 int flag_star;
 int flag_valid_message;

 //BME280 Sensor variables

int g_hum;
int g_press;
int g_temp;

//Acceleration variables

int g_acc_x;
int g_acc_y;
int g_acc_z;

int test;

float current_meas;
float voltage_meas;

double func_timer;
#endif /* GLOBALS_H_ */
