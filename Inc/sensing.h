/*
 * sensing.h
 *
 *  Created on: 12 May 2019
 *      Author: 20901062
 */

#ifndef SENSING_H_
#define SENSING_H_
#include "stm32f3xx_hal_i2c.h"
#include "bme280_defs.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "lis2dh12_reg.h"


int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);
int8_t user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);
void user_delay_ms(uint32_t period);
void i2c_init(I2C_HandleTypeDef i2c);
int8_t stream_sensor_data_normal_mode(struct bme280_dev *dev);
void user_sensing();

int32_t platform_write(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len);
int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len);
void Accelerometer_Initialize(I2C_HandleTypeDef i2c);
void stream_acceleration();

#endif /* SENSING_H_ */
