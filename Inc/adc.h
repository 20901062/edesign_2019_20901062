/*
 * adc.h
 *
 *  Created on: 24 Apr 2019
 *      Author: 20901062
 */

#ifndef ADC_H_
#define ADC_H_

void adc_init(ADC_HandleTypeDef *hadc1, ADC_HandleTypeDef *hadc2);
void adc_loop(ADC_HandleTypeDef *hadc1, ADC_HandleTypeDef *hadc2);


#endif /* ADC_H_ */
