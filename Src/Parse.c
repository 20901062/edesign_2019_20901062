/*
 * Parse.c
 *
 *  Created on: 07 Mar 2019
 *      Author: 20901062
 */
//#include "UART.h"
#include "main.h"
#include "stm32f3xx_it.h"
#include "Globals.h"
#include "lcd.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

uint8_t valid_GPGGA [5] = {'G','P','G','G','A'};
int checkSumDebug = 0;
int calculatedChcksum = 0;
float parse_current;

void parse_init(){

}

void parse_main(){

	int c_counter = 0;

	if(arr_length(save) >=  6){//When the first 6 characters have been received, check that we, indeed, have a GPGGA message
		if(cmp_arr(valid_GPGGA, save, 1, 5) == 1){
			flag_GPGGA = 1;
		}
	}

	if(arr_length(chksum) >= 2){
		checkSumDebug = hex2chksum(chksum);

	}

	if(save[0] == '$' && flag_star == 1){ //Check that the message is complete
		flag_fullmsg = 1;
	}

	if(flag_fullmsg == 1){ //Calculate the checksum of the message
		calculatedChcksum = calc_checksum(save);
	}

	if(flag_GPGGA == 1 && calculatedChcksum == checkSumDebug && flag_fullmsg == 1){//Notify compiler that a complete, valid, GPGGA message has been received. Also copy this message to backup array
		flag_valid_message = 1;

		for(int i = 0; i < arr_length(save); i++){
			bckup[i] = save[i];
		}

		flag_GPGGA = 0; //Reset all flags so receiving process can continue normally
		flag_fullmsg = 0;
	}

	if(flag_valid_message == 1){//Valid message received, commence actual parsing

		int comma_pos = 0;

		for(int k = 0; k < arr_length(bckup); k++){

			if(bckup[k] == ','){//Check for commas
				c_counter++;
				k = k + 1;
				comma_pos = k;
			}

			if(c_counter == 1){ //Save the time
				t[k - comma_pos] = bckup[k];
			}

			if(c_counter == 2){//Save latitude
				lat[k - comma_pos] = bckup[k];
			}

			if(c_counter == 3){//Check North/South
				if(bckup[k] == 'N'){
					N = 1;
				}
				else{
					N = -1;
				}
			}

			if(c_counter == 4){//Save longitude
				lon[k - comma_pos] = bckup[k];
			}

			if(c_counter == 5){//Check East/West
				if(bckup[k] == 'E'){
					E = 1;
				}
				else{
					E = -1;
				}
			}

			if(c_counter == 9){
				alt[k - comma_pos] = bckup[k];
			}

		}

		//Latitude to degrees (This actually works!)
		uint8_t latd[2];
		uint8_t latm[7];
		latd[0] = lat[0];
		latd[1] = lat[1];
		for(int i = 2; i < arr_length((uint8_t*)lat); i++){
			if(lat[i] != '\0'){
			latm[i - 2] = lat[i];
			}
		}

		//Longitude to degrees
		uint8_t lond[3];
		uint8_t lonm[7];
		lond[0] = lon[0];
		lond[1] = lon[1];
		lond[2] = lon[2];
		for(int i = 3; i < arr_length((uint8_t*)lon); i++){
			if(lon[i] != '\0'){
			lonm[i - 3] = lon[i];
			}
		}

		//Convert everything to floats
		latf = (strtod((char*)latd, NULL) + (strtod((char*)latm, NULL)/60))*N;
		lonf = (strtod((char*)lond, NULL) + (strtod((char*)lonm, NULL)/60))*E;
		altf = strtod((char*)alt, NULL);
		flag_valid_message = 0;

	}


}

void parse_output(int runtime, int cur, float vol){

	//parse_current = current;

	sprintf(transmit, "$20901062,%5d,%c%c:%c%c:%c%c,%3d,%3d,%3d,%4d,%4d,%4d,%10.6f,%11.6f,%7.1f,%3d,%3.1f\n",
			runtime, t[0], t[1], t[2], t[3], t[4], t[5], g_temp, g_hum, g_press, g_acc_x, g_acc_y, g_acc_z,
			latf, lonf, altf, cur, vol);//Format output string


}

