/*
 * lcd.c
 *
 *  Created on: 24 Apr 2019
 *      Author: 20901062
 */

#include "main.h"
#include "stm32f3xx_it.h"
#include "Globals.h"

//Pinout configurations

/*
 * PB2 = RS
 * PB1 = RNW
 * PB15 = E
 * PB14 = DB4
 * PB13 = DB5
 * PB5 = DB6
 * PB4 = DB7
 */

void lcd_enable_disable(){
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, 1);
	HAL_Delay(1);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, 0);
}

void lcd_write_to_pins(int RS, int RNW, int DB4, int DB5, int DB6, int DB7){//Shorthand function for quickly setting pin values
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, RS);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, RNW);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, DB7);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, DB6);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, DB5);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, DB4);
	lcd_enable_disable();
}

void lcd_init(){//Initialise LCD according to instructions
	burn_char = ' ';
	HAL_Delay(20);
	lcd_write_to_pins(0, 0, 0, 0, 1, 1);
	HAL_Delay(5);
	lcd_write_to_pins(0,0,0,0,1,1);
	HAL_Delay(5);
	lcd_write_to_pins(0,0,0,0,1,1);
	HAL_Delay(2);
	lcd_write_to_pins(0,0,0,0,1,1);
	lcd_write_to_pins(0,0,0,0,1,0);
	lcd_write_to_pins(0,0,0,0,1,0);
	lcd_write_to_pins(0,0,1,0,0,0);
	lcd_write_to_pins(0,0,0,0,0,0);
	lcd_write_to_pins(0,0,1,0,0,0);
	lcd_write_to_pins(0,0,0,0,0,0);
	lcd_write_to_pins(0,0,0,0,0,1);
	lcd_write_to_pins(0,0,0,0,0,0);
	lcd_write_to_pins(0,0,0,1,1,0);
	lcd_write_to_pins(0,0,0,0,0,0);
	lcd_write_to_pins(0,0,1,1,1,0);
}


void lcd_charHandler(char c){
	uint8_t msb_1 = (c >> 7) & 0x01;//Binary Conversion
	uint8_t msb_2 = (c >> 6) & 0x01;
	uint8_t msb_3 = (c >> 5) & 0x01;
	uint8_t msb_4 = (c >> 4) & 0x01;

	uint8_t lsb_1 = (c >> 3) & 0x01;
	uint8_t lsb_2 = (c >> 2) & 0x01;
	uint8_t lsb_3 = (c >> 1) & 0x01;
	uint8_t lsb_4 = (c) & 0x01;

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, 1);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, 0);

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, msb_4);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, msb_3);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, msb_2);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, msb_1);

	lcd_enable_disable();

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, lsb_4);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, lsb_3);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, lsb_2);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, lsb_1);

	lcd_enable_disable();

}

void lcd_stringHandler(char* s){
	lcd_write_to_pins(0,0,1,0,0,0);
	lcd_write_to_pins(0,0,0,0,0,0);
	for(int i = 0; i <= 7; i++){
		lcd_charHandler(s[i]);
	}

	lcd_write_to_pins(0,0,1,1,0,0);
	lcd_write_to_pins(0,0,0,0,0,0);

	for(int i = 8; i <= 15 ;i++){
		lcd_charHandler(s[i]);
	}
}

void lcd_displayHandler(){
	char lcd_string[15];
	int alt = (int)altf;
	char b = burn_char;

	sprintf(lcd_string, "%-5d   %c   %3dC", alt, b, (int)g_temp);
	int space_c = 0;
	while(lcd_string[space_c] != ' '){
		space_c++;
	}
	lcd_string[space_c] = 'm';

	lcd_stringHandler(lcd_string);
}

