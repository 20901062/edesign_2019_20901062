/*
 * UART.c
 *
 *  Created on: 05 Mar 2019
 *      Author: 20901062
 */
#include "main.h"
#include "stm32f3xx_it.h"
#include "UART.h"
#include "Parse.h"
#include "Globals.h"
#include "lcd.h"

int last_tick;
int current_tick;
int receive_flag = 0;

uint8_t buffer = 0;
int count = 0;
int a = 0;

int RUN_TIME = 0;

int burn_flag = 0;
int burn_counter = 0;
int burn_start = 0;

void user_uart_init(UART_HandleTypeDef *huart1){
	sprintf(transmit, "$20901062,%5d,00:00:00,  0,  0,  0,   0,   0,   0,  0.000000,   0.000000,    0.0,  0,0.0\n", RUN_TIME);
	HAL_UART_Receive_IT(huart1, &buffer, 1);
	flag_fullmsg = 0;
	flag_GPGGA = 0;
	flag_star = 0;
	last_tick = 0;
	receive_flag = 0;
	flag_valid_message = 0;

	for(int i = 0; i < 100; i++){ //Initialise save array
		save[i] = '\0';
	}

	N = 1;
	E = 1;
	burn_counter = 0;

	for(int k = 0; k < sizeof(t); k++){
		t[k] = '0';
	}

	latf = 0.0;
	lonf = 0.0;
	altf = 0.0;

}

void user_uart(UART_HandleTypeDef * huart1){ //This has now become my main loop... It works so I'm not gonna fiddle with it now

	lcd_displayHandler();

	current_tick = HAL_GetTick();

	RUN_TIME = HAL_GetTick()/1000; //Find the runtime in s

	parse_main();

	if(a > 1){
		//count = 0;
		flag_star = 0;
		a = 0;
	}

	if((current_tick - last_tick) >= 1000){

		int cur = (int)current;
		float vol = voltage;

		last_tick = current_tick;
		parse_output(RUN_TIME, cur, vol);

		HAL_UART_Transmit_IT(huart1, (uint8_t*)transmit, arr_length((uint8_t*)transmit));
	}

	if(receive_flag == 1) {//Echo for debugging
		receive_flag = 0;
	}


	//Burn signal things
	if((flag_fullmsg == 1) && (burn_flag == 0)){//Check out of bounds conditions for each full message received
		if((altf > 10000) && ((lonf < 17.976343f) || (lonf > 18.9354f))){
			burn_counter++;
		}else{
			burn_counter = 0;
		}
	}

	if(burn_counter >= 5){
		burn_flag = 1;
		burn_counter = 0;
	}

	if(burn_flag == 1){
		burn_start = HAL_GetTick();
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, SET);
		burn_flag = 2;
	}

	deactivate_burn_signal();

}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef* huart1){ //Transmit Callback

}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart1){ //Recieve Callback

	if(buffer == '$'){ //Check for start of new message
		count = 0;
		flag_star = 0;
	}

	if(buffer == '*'){ //Check for checksum indicator
		flag_star = 1;
	}

	if(flag_star == 0){ //Receive normally

		save[count] = buffer;
		count++;
		receive_flag = 1;
		buffer = 0;
		HAL_UART_Receive_IT(huart1, &buffer, 1);

	}else if(flag_star == 1){ //Receive checksum only

		if(buffer != '*'){

			chksum[a] = buffer;
			a++;
			receive_flag = 1;
			buffer = 0;
			HAL_UART_Receive_IT(huart1, &buffer, 1);

		}else{
			receive_flag = 1;
			buffer = 0;
			HAL_UART_Receive_IT(huart1, &buffer, 1);
		}
	}

}

void deactivate_burn_signal(){
	if(burn_flag == 2){
		if(HAL_GetTick() - burn_start >= 10000){
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, RESET);
			burn_flag = 0;
			burn_counter = 0;
			parse_output(RUN_TIME, (int)current, voltage);
		}
	}
}
