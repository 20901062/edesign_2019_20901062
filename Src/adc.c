/*
 * adc.c
 *
 *  Created on: 24 Apr 2019
 *      Author: 20901062
 */

#include "main.h"
#include "stm32f3xx_it.h"
#include "Globals.h"

float voltage_conv = 4.22307;
float current_conv = (36.70588/2)*0.91*1.18;

int v_counter = 0;
int c_counter = 0;

float sum_voltage = 0;
float sum_current = 0;



void adc_init(ADC_HandleTypeDef * hadc1, ADC_HandleTypeDef * hadc2){


}

void adc_loop(ADC_HandleTypeDef * hadc1, ADC_HandleTypeDef *hadc2){
	int current_tick = 0;
	int last_tick = 0;
	current_2 = (int)current;

	current_tick = HAL_GetTick();

	if((current_tick - last_tick) >= 20){//Take 50 samples per second
		HAL_ADC_Start(hadc1);
		HAL_ADC_Start(hadc2);

		if(HAL_ADC_PollForConversion(hadc1, 5)==HAL_OK){//Voltage samples
			step_voltage[v_counter] = HAL_ADC_GetValue(hadc1);
			v_counter++;
		}

		if(HAL_ADC_PollForConversion(hadc2, 5)==HAL_OK){//Current samples
			step_current[c_counter] = HAL_ADC_GetValue(hadc2);
			c_counter++;
		}

		if(v_counter > 49){
			voltage = 0;

			for(int i = 0; i < 50; i++){
				sum_voltage += step_voltage[i];
			}

			voltage_meas = sum_voltage/50;//For now

			voltage = (sum_voltage/50)*(3.3/4096.0); //Find average values and save in global variables

			voltage *= voltage_conv;//Convert voltage
			//Reset counters & sums
			sum_voltage = 0;
			v_counter = 0;
		}

		if(c_counter > 49){
			current = 0;

			for(int i = 0; i < 50; i++){
				sum_current += step_current[i];
			}

			current_meas = sum_current/50; //For now

			current = (sum_current/50)*(3.6/4096.0)*current_conv; //ADC resolution

			//Reset counters & sums
			sum_current = 0;
			c_counter = 0;
		}

		last_tick = current_tick;

	}
}

