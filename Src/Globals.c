/*
 * Globals.c
 *
 *  Created on: 29 Mar 2019
 *      Author: 20901062
 */

#include "main.h"
#include "stm32f3xx_it.h"
#include "UART.h"
#include "Parse.h"
#include "Globals.h"
#include "sensing.h"
#include<time.h>

int cmp_arr(uint8_t * valid, uint8_t * input, int start, int end){ //Function to compare a populated array with a predefined validation array
	for(int i = start; i < end; i++){
		if(valid[i - start] != input[i]){
			return 0;
		}
	}
	return 1;
}

void clear_arr(uint8_t * arr){ //Function to clear any populated array
	int i = 0;
	while (arr[i] != '\0'){
		arr[i] = '\0';
		i++;
	}
	i = 0;
}

int arr_length(uint8_t * arr){ //Function to calculate the populated length of an array
	int i = 0;
	while (arr[i] != '\0'){
		i++;
	}
	return i;
}

int calc_checksum(uint8_t * arr){//Calculate the checksum of the actual message
	int c = arr[1];

	for(int i = 2; i < arr_length(arr); i++){
		if(arr[i] != '\n' && arr[i] != '\r'){
		c = c ^ arr[i];
		}
	}

	return c;
}

int hex2chksum (uint8_t * arr){ //Convert the hexadecimal checksum to an integer form

	int a [2];

	for(int i = 0; i < 2; i++){
		int n = arr[i];

		if(n >= 48 && n <= 57){
			a[i] = n - 48;
		}

		else if(n >= 65 && n <= 70){
			a[i] = n - 65 + 10;
		}

	}

	return ((a[0] * 16) + a[1]);
}

int ascii_to_int(uint8_t n){
	return (int)n - 48;
}

float str_to_float(uint8_t * arr){

	int point = 0;
	int i = 0;

	float return_val = 0;

	while(arr[point] != '.'){ //Find the position of the decimal point
		point++;
	}

	for(i = (point - 1); i >= 0; i--){
		return_val += (ascii_to_int(arr[i]) * 10^(point - 1 - i));
	}

	for(i = (point + 1); i < arr_length(arr); i++){
		if(arr[i] != '\0'){
		return_val += (ascii_to_int(arr[i]) * 10^(-1*(i - point)));
		}
	}

	return return_val;

}

