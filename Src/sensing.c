/*
 * sensing.c
 *
 *  Created on: 12 May 2019
 *      Author: 20901062
 */

//Includes

#include "stm32f3xx.h"
#include "stm32f3xx_hal.h"
#include "sensing.h"
#include "bme280.h"
#include "bme280_defs.h"
#include "Globals.h"
#include <stdlib.h>
#include <stdio.h>
#include "main.h"
#include "lis2dh12_reg.h"

lis2dh12_ctx_t dev_Accelerometer;
uint8_t whoamI;

uint8_t d_add;

axis3bit16_t data_raw_acceleration;
float acceleration_mg[3];

lis2dh12_reg_t reg;

int reader;
int i;
I2C_HandleTypeDef local_i2c;
struct bme280_dev dev_bme280;

uint32_t hum;
int32_t temp;
uint32_t press;

void i2c_init(I2C_HandleTypeDef i2c){

	local_i2c = i2c;

	dev_bme280.dev_id = BME280_I2C_ADDR_PRIM;
	dev_bme280.intf = BME280_I2C_INTF;
	dev_bme280.read = user_i2c_read;
	dev_bme280.write = user_i2c_write;
	dev_bme280.delay_ms = user_delay_ms;
	bme280_init(&dev_bme280);

	Accelerometer_Initialize(i2c);

}

int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len)
{
	int8_t r = 0; /* Return 0 for Success, non-zero for failure */

	uint8_t data;
	data = reg_addr;


	HAL_I2C_Master_Transmit(&local_i2c,(dev_id<<1) , &data, 1, 100);
	HAL_I2C_Master_Receive(&local_i2c, (dev_id<<1) , reg_data, len, 100);


	return r;
}

int8_t user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len)
{
	int8_t r = 0;

	uint8_t *data1;
	data1 = malloc(len + 1);
	data1[0] = reg_addr;
	memcpy(data1+1, reg_data, len);

	HAL_I2C_Master_Transmit(&local_i2c,(dev_id<<1), (uint8_t*)data1, len + 1, 100);
	free(data1);


	return r;
}

void user_delay_ms(uint32_t period)
{

	HAL_Delay(period);

}


int8_t stream_sensor_data_normal_mode(struct bme280_dev *dev)
{
	int8_t r;
	uint8_t settings_sel;
	struct bme280_data comp_data;

	/* Recommended mode of operation: Indoor navigation */
	dev->settings.osr_h = BME280_OVERSAMPLING_1X;
	dev->settings.osr_p = BME280_OVERSAMPLING_16X;
	dev->settings.osr_t = BME280_OVERSAMPLING_2X;
	dev->settings.filter = BME280_FILTER_COEFF_16;
	dev->settings.standby_time = BME280_STANDBY_TIME_62_5_MS;

	settings_sel = BME280_OSR_PRESS_SEL;
	settings_sel |= BME280_OSR_TEMP_SEL;
	settings_sel |= BME280_OSR_HUM_SEL;
	settings_sel |= BME280_STANDBY_SEL;
	settings_sel |= BME280_FILTER_SEL;
	r = bme280_set_sensor_settings(settings_sel, dev);
	r = bme280_set_sensor_mode(BME280_NORMAL_MODE, dev);

	dev->delay_ms(40);
	r = bme280_get_sensor_data(BME280_ALL, &comp_data, dev);

	temp = comp_data.temperature / 100;		// Divide by 100 for device accuracy.
	press =  comp_data.pressure / 100000; 		// Divide by 100 for device accuracy. Divide again by 1000 for kPa.
	hum = comp_data.humidity / 1024;			// Divide by 1024 for device accuracy.

	g_temp = (int)temp;
	g_press = (int)press;
	g_hum = (int)hum;

	return r;
}

int32_t platform_write(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len)
{
	/* Write multiple command */
	reg |= 0x80;
	HAL_I2C_Mem_Write(handle, LIS2DH12_I2C_ADD_L, reg, I2C_MEMADD_SIZE_8BIT, bufp, len, 100);
	return 0;
}

int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len)
{
	/* Read multiple command */
	reg |= 0x80;
	HAL_I2C_Mem_Read(handle, LIS2DH12_I2C_ADD_L, reg, I2C_MEMADD_SIZE_8BIT, bufp, len, 100);
	return 0;
}

void Accelerometer_Initialize(I2C_HandleTypeDef i2c){

	test = 0;

	dev_Accelerometer.write_reg = platform_write;
	dev_Accelerometer.read_reg = platform_read;
	dev_Accelerometer.handle = &i2c;
	/*
	 *  Check device ID
	 */
	lis2dh12_device_id_get(&dev_Accelerometer, &whoamI);

	if (whoamI != LIS2DH12_ID)
	{
		while(1)
		{
			test = 1;
		}
	}

	/*
	 *  Enable Block Data Update
	 */
	lis2dh12_block_data_update_set(&dev_Accelerometer, PROPERTY_ENABLE);

	/*
	 * Set Output Data Rate to 10Hz
	 */
	lis2dh12_data_rate_set(&dev_Accelerometer, LIS2DH12_ODR_10Hz);

	/*
	 * Set full scale to 2g
	 */
	lis2dh12_full_scale_set(&dev_Accelerometer, LIS2DH12_2g);

	/*
	 * Enable temperature sensor
	 */
	lis2dh12_temperature_meas_set(&dev_Accelerometer, LIS2DH12_TEMP_ENABLE);

	/*
	 * Set device in continuous mode with 12 bit resol.
	 */
	lis2dh12_operating_mode_set(&dev_Accelerometer, LIS2DH12_HR_12bit);




}

void stream_acceleration(I2C_HandleTypeDef i2c){
	dev_Accelerometer.write_reg = platform_write;
	dev_Accelerometer.read_reg = platform_read;
	dev_Accelerometer.handle = &i2c;


	lis2dh12_xl_data_ready_get(&dev_Accelerometer, &reg.byte);
	HAL_Delay(1);
	/*
	 * Read output only if new value available
	 */
	if (reg.byte)
	{
		/* Read accelerometer data */
		memset(data_raw_acceleration.u8bit, 0x00, 3*sizeof(int16_t));
		HAL_Delay(1);
		lis2dh12_acceleration_raw_get(&dev_Accelerometer, data_raw_acceleration.u8bit);
		HAL_Delay(1);
		acceleration_mg[0] = lis2dh12_from_fs2_hr_to_mg(data_raw_acceleration.i16bit[0]);
		acceleration_mg[1] = lis2dh12_from_fs2_hr_to_mg(data_raw_acceleration.i16bit[1]);
		acceleration_mg[2] = lis2dh12_from_fs2_hr_to_mg(data_raw_acceleration.i16bit[2]);

		g_acc_z = -(int)acceleration_mg[0]; 		//store acceleration in the board specific global variables.
		g_acc_y = (int)acceleration_mg[1] + 40;

		/* The offset value for the y acceleration was experimentally determined by taking a range of values and finding the average.
		 * This had to be done due to a header being used to hold the accelerometer (practical purposes),
		 * but the header was not properly soldered. Thus, an offset needs to be added in
		 * order to get accurate readings from the accelerometer
		 */

		g_acc_x = -(int)acceleration_mg[2];

		if (g_acc_y >= 999) g_acc_y = 999;
		if (g_acc_y <= -999) g_acc_y = -999;
		if (g_acc_z >= 999) g_acc_z = 999;
		if (g_acc_z <= -999) g_acc_z = -999;
		if (g_acc_x >= 999) g_acc_x = 999;
		if (g_acc_x <= -999) g_acc_x = -999;



	}

}

void user_sensing(){

	int current_tick = 0;
	int last_tick = 0;

	current_tick = HAL_GetTick();

	//Take 2 samples from the both sensors per second to ensure accuracy

	if((current_tick - last_tick) >= 500){
		stream_sensor_data_normal_mode(&dev_bme280);
		stream_acceleration(local_i2c);
	}

}
